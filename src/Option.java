public abstract class Option {
    
    private double strike;
    private double maturity;


    public double getMaturity() {
        return maturity;
    }

    public double getStrike() {
        return strike;
    }
}
