public class MonteCarlo {
    
    private int nbTries;
    private double timeStep;

    public int getNbTries() {
        return nbTries;
    }

    public double getTimeStep() {
        return timeStep;
    }
}
