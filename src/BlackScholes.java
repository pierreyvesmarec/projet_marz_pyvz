public class BlackScholes {
    
    private double[] spots; 
    private double period;
    private double r;
    private double[] volatility;
    private int nbTimeStep;

    BlackScholes(double period, double r, double volatility[], int nbTimeStep, double[] spots) {
        this.spots = spots;
        this.period = period;
        this.r = r;
        this.volatility = volatility;
        this.nbTimeStep = nbTimeStep;
    }

    public double getR() {
        return r;
    }

    public double[] getVolatility() {
        return volatility;
    }

    public double getPeriod() {
        return period;
    }

    public int getNbTimeStep() {
        return nbTimeStep;
    }

    public double[] getSpots() {
        return spots;
    }

    public double[][] asset(int nbTimeStep, double period, double r, double volatility){
        int n = spots.length;
        double[][] values = new double[n][nbTimeStep];
        for (int k = 0; k < n; k++) {
            values[k][0] = spots[k];
        }
        double dt = period / (nbTimeStep+1);
        for (int i = 0; i < n; i++){
            for (int j = 1; j < nbTimeStep+1; j++) {
                values[i][j] = 0;
            } 
        }    
        return values;
    }
}
