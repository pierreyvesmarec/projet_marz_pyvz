import numpy as np
from scipy.optimize import minimize
import Data as dt

def optimize_portfolio_sharpe(expected_returns, cov_matrix, risk_free_rate):
    num_assets = len(cov_matrix)  # Nombre d'actifs est égal au nombre de lignes dans la matrice de covariance

    # Définir une fonction objectif pour maximiser le ratio de Sharpe
    def objective(weights):
        weights = np.array(weights).reshape(-1, 1)
        portfolio_return = np.dot(weights.T, expected_returns)
        portfolio_std_dev = np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights)))
        sharpe_ratio = (portfolio_return) / portfolio_std_dev
        return -sharpe_ratio  # Négatif car nous voulons maximiser

    # Contrainte pour s'assurer que la somme des poids est égale à 1
    def constraint(weights):
        return np.sum(weights) - 1.0

    # Définir les limites de poids (de 0 à 1)
    bounds = tuple((0, 1) for asset in range(num_assets))

    # Définir la contrainte
    constraints = ({'type': 'eq', 'fun': constraint})

    # Initialiser les poids de manière égale pour commencer
    initial_weights = np.array(num_assets * [1.0 / num_assets])

    # Minimiser la fonction objectif (maximiser le ratio de Sharpe)
    optimized_portfolio = minimize(objective, initial_weights, method='SLSQP', bounds=bounds, constraints=constraints)

    return optimized_portfolio.x

# Exemple d'utilisation :
expected_returns = dt.rendementsCAC().mean()  # Rendements moyens des actifs
cov_matrix = dt.covCAC(dt.rendementsCAC())  # Matrice de covariance des actifs
risk_free_rate = 0.03  # Taux sans risque

weights = optimize_portfolio_sharpe(expected_returns, cov_matrix, risk_free_rate)

# Récupérer les noms des actions
actions = dt.getDataCAC(1).columns

# Seuil pour les poids faibles
seuil_poids_faible = 0.005  # Par exemple, un seuil de 5%

# Afficher les noms des actions et les poids finaux, en remplaçant les poids faibles par 0
for action, weight in zip(actions, weights):
    if weight < seuil_poids_faible:
        weight = 0
    print("Action :", action, ", Poids final :", weight)
