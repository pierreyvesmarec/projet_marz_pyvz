# finance_data.py

from sqlite3 import Time
import yfinance as yf
import pandas as pd
import numpy as np

def getDataCAC(time_period):
    cac40_symbols = ["AIR.PA", "AI.PA", "BNP.PA", "CS.PA", "CAP.PA", "CA.PA", "ACA.PA", "BN.PA", "ENGI.PA", "DSY.PA",
                    "KER.PA", "OR.PA", "LR.PA", "MC.PA", "MT.AS", "ML.PA", "ORA.PA", "RI.PA", "PUB.PA", "RNO.PA",
                    "SAF.PA", "SGO.PA", "SAN.PA", "SU.PA", "GLE.PA", "SW.PA", "TEP.PA", "HO.PA", "VIV.PA", "DG.PA"]

    debut_periode = pd.to_datetime("today") - pd.DateOffset(years=time_period)
    fin_periode = pd.to_datetime("today")

    donnees_actions_cac40 = yf.download(cac40_symbols, start=debut_periode, end=fin_periode)

    return donnees_actions_cac40

def getDataSNP500():
    url_sp500_components = 'https://en.wikipedia.org/wiki/List_of_S%26P_500_companies'
    sp500_table = pd.read_html(url_sp500_components, header=0)[0]
    sp500_symbols = sp500_table['Symbol'].tolist()
    debut_periode = pd.to_datetime("today") - pd.DateOffset(years=1)
    fin_periode = pd.to_datetime("today")

    donnees_actions_sp500 = yf.download(sp500_symbols, start=debut_periode, end=fin_periode)
    
    return donnees_actions_sp500

def rendementsCAC():
    dataCAC = getDataCAC(1)
    adj_close_data = dataCAC["Adj Close"]
    returns = adj_close_data.pct_change()

    return returns

def covCAC(returns):
    cov = returns.cov()
    return cov
